package com.leroy.springbootsample;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("test")
@Primary
public class SampleServiceMocked implements SampleService{

	@Override
	public String hello() {
		return "hello mocked !";
	}

}
