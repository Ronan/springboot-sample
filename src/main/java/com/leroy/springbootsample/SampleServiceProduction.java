package com.leroy.springbootsample;

import org.springframework.stereotype.Service;

@Service
public class SampleServiceProduction implements SampleService{

	@Override
	public String hello() {
		return "hello production !";
	}

}
