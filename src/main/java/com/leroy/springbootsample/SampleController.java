package com.leroy.springbootsample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class SampleController {

	@Autowired
	private SampleService service;
	
    @RequestMapping("/")
    @ResponseBody
    public String home() {
        return service.hello();
    }

}
