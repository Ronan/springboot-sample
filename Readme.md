# Sample Springboot Application

## Configuring service for production and for test

By default, using the command `java -jar target/springboot-sample-*.jar`,
`SampleServiceProduction` is used (no annotation needed).

By default, using the command `java -Dspring.profiles.active=test -jar target/springboot-sample-*.jar`,
`SampleServiceMocked` is used :
- @Profile("test") : This bean will be registered only if profile test is activated. 
- @Primary : As both bean will be activated, tells Spring this bean is to be chosen.
